class CreateMurids < ActiveRecord::Migration[6.0]
  def up
    create_table :murids do |t|
      t.string :nama
      t.integer :nis
      t.string :alamat

      t.timestamps
    end
  end
  def down
    drop_table = murids
end
end
