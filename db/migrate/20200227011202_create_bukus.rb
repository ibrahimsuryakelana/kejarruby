class CreateBukus < ActiveRecord::Migration[6.0]
  def up
    create_table :bukus do |t|
      t.string :title
      t.string :name
      t.integer :page

      t.timestamps
    end
  end
  def down 
    drop_table = bukus
end
