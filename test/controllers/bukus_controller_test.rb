require 'test_helper'

class BukusControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get bukus_new_url
    assert_response :success
  end

  test "should get create" do
    get bukus_create_url
    assert_response :success
  end

  test "should get edit" do
    get bukus_edit_url
    assert_response :success
  end

  test "should get update" do
    get bukus_update_url
    assert_response :success
  end

  test "should get destroy" do
    get bukus_destroy_url
    assert_response :success
  end

  test "should get index" do
    get bukus_index_url
    assert_response :success
  end

  test "should get show" do
    get bukus_show_url
    assert_response :success
  end

end
