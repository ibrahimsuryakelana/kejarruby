class MuridsController < ApplicationController
  def new
    @murid = Murid.new
  end

  def create
    murid = Murid.new(resource_params)
    murid.save
    flash[:notice] = "murid has been created"
  end

  def edit
    @murid = Murid.find(params[:id])
  end

  def update
    @murid = Murid.find(params[:id])
    @murid.update(resource_params)
    flash[:notice] = "murid has been Updated"
    redirect_to murids_path(@murid)
  end

  def destroy
    @murid = Murid.find(params[:id])
    @murid.destroy
    flash[:notice] = "murid has been Deleted"
    redirect_to murids_path
  end

  def index
    @murids = Murid.all
  end

  def show
    id = params[:id]
    @murid = Murid.find(id)
  end

  private
  def resource_params
    params.require(:murid).permit(:nama, :integer, :alamat)
  end
end
