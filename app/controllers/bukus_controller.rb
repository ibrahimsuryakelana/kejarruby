class BukusController < ApplicationController
  def new
    @buku = Buku.new
  end

  def create
    buku = Buku.new(resource_params)
    buku.save
    flash[:notice] = "buku has been created"
  end

  def edit
    @buku = Buku.find(params[:id])
  end

  def update
    @buku = Buku.find(params[:id])
    @buku.update(resource_params)
    flash[:notice] = "buku has been Updated"
    redirect_to bukus_path(@buku)
  end

  def destroy
    @buku = Buku.find(params[:id])
    @buku.destroy
    flash[:notice] = "buku has been Deleted"
    redirect_to bukus_path
  end

  def index
    @bukus = Buku.all
  end

  def show
    id = params[:id]
    @buku = Buku.find(id)
  end

  private
  def resource_params
    params.require(:buku).permit(:titlr, :page, :price, :description)
  end
end
